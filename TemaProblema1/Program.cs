﻿using System;

namespace TemaProblema1
{
    class Program
    {
        static void Main(string[] args)
        {
            //variabile de baza
            Console.WriteLine("Introduceti n");
            int n = int.Parse(Console.ReadLine());
            int[] v = new int[n];
            int media = 0;
            int count = 0;
            //ciclu pentru preluarea valorilor
            for(int i = 0; i < v.Length; i++)
            {
                v[i] = int.Parse(Console.ReadLine());
            }
            Console.WriteLine("Introduceti a");
            int a = int.Parse(Console.ReadLine());
            Console.WriteLine("Introduceti b");
            int b = int.Parse(Console.ReadLine());
            //control max var
            if (a > b)
            {
                int temp = b;
                b = a;
                a = temp;

            }
            //ciclu pentru calcul medie
            for(int i = 0;i< v.Length; i++)
            {
                if(v[i]>a && v[i] < b)
                {
                    media += v[i];
                    count++;
                }
            }
            Console.WriteLine("Media este:" + media / count);
        }
    }
}
